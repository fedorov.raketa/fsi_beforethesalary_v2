import 'package:flutter/material.dart';
import 'package:flutter_application_1/calculate.dart';

class BlockTotalSumma extends StatefulWidget {
  Calculate calc;
  BlockTotalSumma(this.calc, {super.key});

  @override
  State<BlockTotalSumma> createState() => _BlockTotalSummaState(calc);
}

class _BlockTotalSummaState extends State<BlockTotalSumma> {
  Calculate calc;
  _BlockTotalSummaState(this.calc);
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 10),
      child: Container(
        alignment: Alignment.centerLeft,
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text("Введите сумму для расчёта:"),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              width: 250,
              child: TextField(
                controller: _controller,
                textAlign: TextAlign.center,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Сумма',
                  icon: Icon(
                    Icons.account_balance_wallet_rounded,
                    size: 40,
                  ),
                ),
                onChanged: (value) {
                  if (_controller.text.isNotEmpty) {
                    int? rez = int.tryParse(_controller.text);
                    if (rez != null) {
                      calc.totalMoney = int.parse(_controller.text);
                    } else {
                      ///todo Сделать вывод сообщения о неверном вводе
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
