import 'package:flutter/material.dart';
import 'package:flutter_application_1/calculate.dart';
import 'supportFunctions.dart';

class BlockResult extends StatelessWidget {
  Calculate calc;
  BlockResult(this.calc, {super.key});

  @override
  Widget build(BuildContext context) {
    // calc.calculateAllSpends();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Результаты вычислений'),
      ),
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              "assets/imgs/fon.jpg",
              fit: BoxFit.cover,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Card(
                      color: Colors.blue[300],
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Center(
                                child: Text(
                                  'Расход в день:',
                                  style: Theme.of(context).textTheme.bodyText1,
                                  // style: TextStyle(
                                  //   fontWeight: FontWeight.bold,
                                  //   fontSize: 27,
                                  // ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                alignment: Alignment.center,
                                padding: EdgeInsetsDirectional.all(10.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 2.0,
                                    color: Colors.black,
                                  ),
                                  borderRadius: BorderRadius.circular(7.0),
                                ),
                                child: Text(
                                  calc.rezultSummaByEveryDay.toString(),
                                  // style: TextStyle(
                                  //   fontWeight: FontWeight.bold,
                                  //   fontSize: 27,
                                  // ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Card(
                      child: Column(
                        children: [
                          // Заголовок Подробностей
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Подробности расчёта",
                              style: Theme.of(context).textTheme.headline1,
                            ),
                          ),

                          // Кол-во дней
                          printTextRow(
                              "Кол-во дней: ", calc.countDays.toString()),
                          // Text("Кол-во дней: " + calc.countDays.toString()),

                          // Период
                          printTextRow(
                            "Период:",
                            "с " +
                                SupportFunctions.getFormatDate2(
                                    calc.beginDate) +
                                " по " +
                                SupportFunctions.getFormatDate2(calc.endDate),
                          ),

                          //Разовые расходы
                          calc.listOneSpending.isNotEmpty
                              ? ListRashod("Разовый расход:",
                                  calc.listOneSpending, context)
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Разовых расходов нет."),
                                ),

                          //Ежедневные расходы
                          calc.listEveryDaySpending.isNotEmpty
                              ? ListRashod("Ежедневный расход:",
                                  calc.listEveryDaySpending, context)
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Ежедневных расходов нет."),
                                ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printTextRow(String title, String rezult) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(title),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(rezult,
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
            ],
          ),
        ],
      ),
    );
  }

  Widget ListRashod(String title, var listSpends, var context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                title,
                style: Theme.of(context).textTheme.headline2,
              ),
            ),
          ],
        ),
        const Divider(),
        DataTable(
          columnSpacing: 35.0,
          columns: const [
            DataColumn(label: Text("Расход")),
            DataColumn(label: Text("Сумма")),
          ],
          rows: List<DataRow>.generate(
            listSpends.length,
            (index) {
              return DataRow(
                cells: [
                  DataCell(Text(listSpends[index].name)),
                  DataCell(Text(
                    listSpends[index].summa.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                  )),
                ],
              );
            },
          ),
        ),
        const Divider(),
      ],
    );
  }
}
