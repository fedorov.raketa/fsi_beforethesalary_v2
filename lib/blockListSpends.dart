import 'package:flutter/material.dart';
import 'calculate.dart';

class BlockListSpends extends StatefulWidget {
  List<ItemListSpends> listSpends;
  ListSpendsTitles titles;
  BlockListSpends(this.listSpends, this.titles, {super.key});

  @override
  State<BlockListSpends> createState() =>
      _BlockListSpendsState(listSpends, titles);
}

class _BlockListSpendsState extends State<BlockListSpends> {
  ListSpendsTitles titles;
  List<ItemListSpends> listSpends;
  var nameController = TextEditingController();
  var summaController = TextEditingController();

  _BlockListSpendsState(this.listSpends, this.titles);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Text(titles.titleInput),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    // SizedBox(
                    // width: 100,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: nameController,
                        key: const Key("naimField"),
                        keyboardType: TextInputType.text,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Наименование",
                        ),
                        // onChanged: (value) => Name = value,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    // SizedBox(
                    // width: 50,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: summaController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Сумма",
                        ),
                        // onChanged: (value) => summa = value,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        // icon: Icon(Icons.add),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Icon(
                                  // Icons.add_box,
                                  Icons.add_task_rounded),
                            ),
                            Text('Добавить'),
                          ],
                        ),
                        onPressed: () {
                          var itemSpend = ItemListSpends();
                          if (nameController.text.isNotEmpty &&
                              summaController.text.isNotEmpty) {
                            itemSpend.name = nameController.text;
                            itemSpend.summa = int.parse(summaController.text);
                            listSpends.add(itemSpend);
                            setState(() {
                              nameController.clear();
                              summaController.clear();
                            });
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        const SizedBox(height: 5),
        Row(
          children: [
            Expanded(
              child: Card(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Text(titles.titleOunput),
                        ),
                      ],
                    ),
                    const Divider(),
                    DataTable(
                      columnSpacing: 35.0,
                      columns: const [
                        DataColumn(label: Text("Расход")),
                        DataColumn(label: Text("Сумма")),
                        DataColumn(label: Text("Удалить")),
                      ],
                      rows: List<DataRow>.generate(
                        listSpends.length,
                        (index) {
                          return DataRow(
                            cells: [
                              DataCell(Text(listSpends[index].name)),
                              DataCell(
                                  Text(listSpends[index].summa.toString())),
                              DataCell(
                                IconButton(
                                  icon: const Icon(
                                    Icons.delete_forever,
                                  ),
                                  onPressed: () {
                                    listSpends.removeAt(index);
                                    setState(() {});
                                  },
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                          onPressed: () {
                            setState(() {
                              listSpends.clear();
                            });
                          },
                          child: const Text('Очистить список'),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class ListSpendsTitles {
  String titleInput = '';
  String titleOunput = '';
  ListSpendsTitles(this.titleInput, this.titleOunput);
}
