///Calculate - кдасс предназначен для вычисления дат, денег и т.п. в программе

class Calculate {
  /// изначальная сумма в кормане
  int totalMoney = 0;

  /// сумма на каждый день
  int rezultSummaByEveryDay = 0;

  /// дата начала периода
  DateTime _beginDate = DateTime.now();

  /// дата начала периода
  DateTime get beginDate => _beginDate;

  /// дата начала периода
  set beginDate(DateTime beginDate) {
    _beginDate = beginDate;
  }

  /// дата конца периода
  DateTime endDate = DateTime.now();

  /// количество дней между начальной и конечной датой включая
  int _countDays = 0;

  /// количество дней между начальной и конечной датой включая
  int get countDays => _countDays;

  /// количество дней между начальной и конечной датой включая
  set countDays(int countDays) {
    _countDays = countDays;
  }

  /// вычислить и получить количество дней в диапазоне дат
  void calculateCountDays() {
    if (beginDate != endDate) {
      //если диапазон установлен
      countDays = endDate.difference(beginDate).inDays + 1;
    } else {
      countDays = -1;
    }
  }

  ///лист единоразовых расходов
  List<ItemListSpends> _listOneSpending = [];

  ///лист единоразовых расходов
  List<ItemListSpends> get listOneSpending => _listOneSpending;

  ///лист единоразовых расходов
  set listOneSpending(List<ItemListSpends> listOneSpending) {
    _listOneSpending = listOneSpending;
  }

  ///лист ежедневных расходов
  List<ItemListSpends> _listEveryDaySpending = [];

  ///лист ежедневных расходов
  List<ItemListSpends> get listEveryDaySpending => _listEveryDaySpending;

  ///лист ежедневных расходов
  set listEveryDaySpending(List<ItemListSpends> listEveryDaySpending) {
    _listEveryDaySpending = listEveryDaySpending;
  }

  //произвести полный расчёт
  void calculateAllSpends() {
    int summa = totalMoney - _calculateSpendingSumma();
    if (countDays == 0) return;
    int summaByDay = summa ~/ countDays;
    rezultSummaByEveryDay = summaByDay - _calculateSpendingEveryDay();
  }

  ///список расходов на период
  int _calculateSpendingSumma() {
    int summa = 0;
    if (listOneSpending.isNotEmpty) {
      for (var item in listOneSpending) {
        summa += item.summa;
      }
    }
    return summa;
  }

  ///список расходов на день
  int _calculateSpendingEveryDay() {
    int rezult = 0;
    if (listEveryDaySpending.isNotEmpty) {
      for (var item in listEveryDaySpending) {
        rezult += item.summa;
      }
    }
    return rezult;
  }
}

///объект для списков рсходов
class ItemListSpends {
  String name = "";
  int summa = 0;
}
