import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/date_symbol_data_local.dart';

class SupportFunctions {
  static String getFormatDate(DateTime dt) {
    String str = dt.day.toString() +
        "-" +
        dt.month.toString() +
        "-" +
        dt.year.toString();
    return str;
  }

  static String getFormatDate2(DateTime dt) {
    initializeDateFormatting();
    String formattedDate = DateFormat('d MMMM', 'ru').format(dt);
    return formattedDate;
  }
}
