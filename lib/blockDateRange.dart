import 'package:flutter/material.dart';
import 'supportFunctions.dart';
import 'calculate.dart';

class BlockDataRange extends StatefulWidget {
  Calculate calc;
  BlockDataRange(this.calc, {super.key});

  @override
  State<BlockDataRange> createState() => _BlockDataRangeState(calc);
}

class _BlockDataRangeState extends State<BlockDataRange> {
  late Calculate calc;
  _BlockDataRangeState(this.calc);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: ElevatedButton.icon(
                label: const Text('Выбрать диапазон дат'),
                icon: const Icon(Icons.date_range),
                onPressed: showDateTimeRange,
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Установленный диапазон дат:',
                //style: rezultTextStyle,
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Text("c"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                // color: Colors.green,
                border: Border.all(
                  color: Colors.green,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(10),
                // boxShadow: [
                //   borderShadow,
                // ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  SupportFunctions.getFormatDate(calc.beginDate),
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            Container(
              child: const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Text("по"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                // color: Colors.green,
                border: Border.all(
                  color: Colors.green,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(10),
                // boxShadow: [
                //   borderShadow,
                // ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  SupportFunctions.getFormatDate(calc.endDate),
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  // выбор диапазона дат для подсчёта кол-ва дней
  void showDateTimeRange() async {
    DateTimeRange? dtr = await showDateRangePicker(
      context: context,
      firstDate: DateTime.now(),
      lastDate: DateTime.utc(2030, 1, 1),
    );
    setState(
      () {
        if (dtr != null) {
          calc.beginDate = dtr.start;
          calc.endDate = dtr.end;
          calc.calculateCountDays();
        }
      },
    );
  }
}
