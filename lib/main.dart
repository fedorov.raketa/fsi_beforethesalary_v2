import 'package:flutter/material.dart';

//пользовательский импорт
import 'package:flutter_application_1/calculate.dart';
import 'blockDateRange.dart';
import 'blockListSpends.dart';
import 'blockResult.dart';
import 'blockTotalSumma.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

///model and calculate

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Before the salary',
      theme: ThemeData(
        //brightness: Brightness.dark,
        // fontFamily: 'Algerius',
        cardTheme: CardTheme(
          elevation: 5,
          shadowColor: Color.fromARGB(255, 72, 72, 72),
          color: Color.fromARGB(255, 238, 238, 238),
        ),
        textTheme: const TextTheme(
          bodyText1: TextStyle(
            color: Color.fromARGB(255, 0, 0, 0),
            fontFamily: 'Algerius',
            fontSize: 23,
            shadows: [
              Shadow(
                blurRadius: 5,
                color: Colors.black38,
                offset: Offset(1, 2),
              ),
            ],
          ),
          headline1: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Colors.black,
            shadows: [
              Shadow(
                blurRadius: 5,
                color: Colors.black38,
                offset: Offset(1, 2),
              ),
            ],
          ),
          headline2: TextStyle(
            fontSize: 16,
          ),
        ),
        primarySwatch: Colors.green,
      ),
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate],
      supportedLocales: const [Locale('ru', 'RU')],
      home: const MyHomePage(title: 'До зарплаты'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var calculate = Calculate();
  var _currentStep = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              "assets/imgs/fon.jpg",
              fit: BoxFit.cover,
            ),
            Expanded(
              child: Container(
                color: Color.fromARGB(110, 255, 255, 255),
              ),
            ),
            Stepper(
              type: StepperType.vertical,
              currentStep: _currentStep,
              onStepContinue: () {
                switch (_currentStep) {
                  case 0:
                    _currentStep += 1;
                    break;
                  case 1:
                    _currentStep += 1;
                    break;
                  case 2:
                    _currentStep += 1;
                    break;
                  case 3:
                    calculate.calculateAllSpends();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BlockResult(calculate),
                      ),
                    );
                    break;
                }
                setState(() {});
              },
              onStepCancel: _currentStep > 0
                  ? () => setState(() => _currentStep -= 1)
                  : null,
              onStepTapped: (value) => setState(() {
                _currentStep = value;
              }),
              steps: [
                Step(
                  label: Text('sdf'),
                  title: Container(
                    padding: EdgeInsets.all(3),
                    // decoration: BoxDecoration(
                    //     border: Border.all(
                    //       color: Colors.black,
                    //       width: 2,
                    //     ),
                    //     borderRadius: BorderRadius.circular(5),
                    //     color: Colors.white24),
                    child: const Text(
                      'Оставшаяся сумма',
                    ),
                  ),
                  content: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: BlockTotalSumma(calculate),
                    ),
                  ),
                  isActive: true,
                  state: _currentStep >= 0
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: const Text('Выбор диапазона дат'),
                  content: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: BlockDataRange(calculate),
                    ),
                  ),
                  isActive: true,
                  state: _currentStep >= 1
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: const Text("Расходы"),
                  content: BlockListSpends(
                      calculate.listOneSpending,
                      ListSpendsTitles('Разовые расходы на период:',
                          'Список разовых расходов')),
                  isActive: true,
                  state: _currentStep >= 2
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: const Text("Ежедневный расход"),
                  content: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: BlockListSpends(
                      calculate.listEveryDaySpending,
                      ListSpendsTitles('Ежедневные расходы на период:',
                          'Список ежедневных расходов'),
                    ),
                  ),
                  isActive: true,
                  state: _currentStep >= 3
                      ? StepState.complete
                      : StepState.disabled,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
